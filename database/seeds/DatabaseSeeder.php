<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * --------------------------------------------
     * Use the following commands from iSeed:
     * - php artisan iseed my_table,another_table  // creating seeders for multiple tables
     * - php artisan iseed users --force           // overwrite the exising seed for the table
     * -
     * --------------------------------------------
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);

    }
}
