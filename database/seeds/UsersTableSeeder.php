<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('users')->delete();

        \DB::table('users')->insert(array (
            0 =>
            array (
                'created_at' => '2018-09-23 16:18:20',
                'email' => 'shb182@hotmail.com',
                'email_verified_at' => '2018-09-23 16:18:22',
                'id' => 1,
                'name' => 'Shiblie Bshara',
                'password' => '$2y$10$yQaSRsfLMX2xqWTniqVf1OBIQtmgcz5nRXj4sh4TazuF64uh8hT1i',
                'remember_token' => 'dlESKoo6rwMvsnjJdrqow1DLhnhpCQ8hymSSpufxEqgTzK68jwJ5yyqvczOH',
                'updated_at' => '2018-09-23 16:18:22',
            ),
        ));


    }
}
