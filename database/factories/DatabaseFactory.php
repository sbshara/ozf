<?php

use Faker\Generator as Faker;

    factory(App\Address::class, 500)->create();
    factory(App\Brand::class, 10)->create();
    factory(App\Category::class, 8)->create();
    factory(App\City::class, 5000)->create();
    factory(App\Country::class, 200)->create();
    factory(App\Colour::class, 100)->create();
    factory(App\Customer::class, 500)->create();
    factory(App\Order::class, 500)->create();
    factory(App\Payment::class, 500)->create();
    factory(App\ProductColour::class, 50)->create();
    factory(App\Product::class, 50)->create();
    factory(App\ProductSize::class, 50)->create();
    factory(App\Season::class, 10)->create();
    factory(App\Size::class, 20)->create();
    factory(App\Status::class, 10)->create();
    factory(App\Transaction::class, 500)->create();
    factory(App\Upload::class, 50)->create();
    factory(App\User::class, 50)->create();
    factory(App\Vendor::class, 50)->create();
