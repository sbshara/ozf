<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('amount');
            $table->integer('currency_id')->unsigned();
                // $table->foreign('currency_id')->references('id')->on('currencies');
            $table->integer('transaction_id')->unsigned();
                // $table->foreign('transaction_id')->references('id')->on('transactions');
            $table->integer('user_id')->unsigned();
                // $table->foreign('user_id')->references('id')->on('users');
            $table->integer('status_id')->unsigned();
                // $table->foreign('status_id')->references('id')->on('statuses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
