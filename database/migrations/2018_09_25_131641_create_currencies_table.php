<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code')->nullable();
            $table->string('symbol')->nullable();
            $table->integer('country_id')->unsigned()->nullable();
                // $table->foreign('country_id')->references('id')->on('countries');
            $table->integer('value_in_usd');
            $table->string('currency')->nullable();
            $table->string('currencies')->nullable();
            $table->string('cent')->nullable();
            $table->string('cents')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
    }
}
