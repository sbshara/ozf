<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id')->unsigned();
                // $table->foreign('vendor_id')->references('id')->on('vendors');
            $table->integer('brand_id')->unsigned();
                // $table->foreign('brand_id')->references('id')->on('brands');
            $table->string('name');
            $table->string('style_ref');
            $table->text('description')->nullable();
            $table->integer('season_id')->unsigned();
                // $table->foreign('season_id')->references('id')->on('seasons');
            $table->integer('category_id')->unsigned();
                // $table->foreign('category_id')->references('id')->on('categories');
            $table->integer('user_id')->unsigned();
                // $table->foreign('user_id')->references('id')->on('users');
            $table->integer('cost_amount');
            $table->integer('currency_id')->unsigned();
                // $table->foreign('currency_id')->references('id')->on('currencies');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
