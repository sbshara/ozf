<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_ref');
            $table->integer('transaction_id')->unsigned();
                // $table->foreign('transaction_id')->references('id')->on('transactions');
            $table->integer('product_id')->unsigned();
                // $table->foreign('product_id')->references('id')->on('products');
            $table->integer('size_id')->unsigned();
                // $table->foreign('size_id')->references('id')->on('sizes');
            $table->integer('colour_id')->unsigned();
                // $table->foreign('colour_id')->references('id')->on('colours');
            $table->integer('price');
            $table->integer('currency_id')->unsigned();
            // $table->foreign('currency_id')->references('id')->on('currencies');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
