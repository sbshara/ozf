<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->text('address1')->nullable();
            $table->text('address2')->nullable();
            $table->string('area')->nullable();
            $table->integer('city_id')->unsinged();
                // $table->foreign('ctyi_id')->references('id')->on('cities');
            $table->string('zip_postal')->nullable();
            $table->string('p_o_box')->nullable();
            $table->integer('phone')->nullable();
            $table->integer('customer_id')->unsigned();
                // $table->foreign('customer_id')->references('id')->on('customers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
