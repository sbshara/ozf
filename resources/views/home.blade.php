@extends('layouts.adminLTE')

@section('content')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Dashboard
			<small>Optional description</small>
		</h1>
		@include('layouts.adminLTE.breadCrumb')
	</section>
	<!-- Main content -->
	<section class="content container-fluid">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
    </section>
<form class="form">
    {{-- @include('layouts.fileUpload.dropzone') --}}
    <hr>
    @include('layouts.fileUpload.cropper')
    <hr>
    {{-- @include('layouts.fileUpload.knockout') --}}
</form>

	<!-- /.content -->
</div>
@endsection

@section('additionalCSS')

@endsection

@section('additionalJS')

@endsection


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
