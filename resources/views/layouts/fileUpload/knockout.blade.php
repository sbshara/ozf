@section('additionalCSS')

<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
<link rel='stylesheet' href='https://rawgit.com/adrotec/knockout-file-bindings/master/knockout-file-bindings.css'>
<link rel="stylesheet" href="{{ route('Base') }}/css/knockout.css">

@endsection

<div class="form-group">
    <div class="well" data-bind="fileDrag: multiFileData">
        <div class="form-group row">
            <div class="col-md-6">
                    <!-- ko foreach: {data: multiFileData().dataURLArray, as: 'dataURL'} -->
                    <img style="height: 100px; margin: 5px;" class="img-rounded  thumb" data-bind="attr: { src: dataURL }, visible: dataURL">
                    <!-- /ko -->
                <div data-bind="ifnot: fileData().dataURL">
                    <label class="drag-label">Drag files here</label>
                </div>
            </div>
            <div class="col-md-6">
                <input type="file" multiple data-bind="fileInput: multiFileData, customFileInput: {
                buttonClass: 'btn btn-success',
                fileNameClass: 'disabled form-control',
                onClear: onClear,
                }" accept="image/*">
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <button class="btn btn-default" data-bind="click: debug">Debug</button>
</div>

@section('additionalJS')

<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/knockout/3.1.0/knockout-min.js'></script>
<script src='https://rawgit.com/adrotec/knockout-file-bindings/master/knockout-file-bindings.js'></script>
<script  src="{{ route('Base') }}/js/knockout.js"></script>

@endsection
