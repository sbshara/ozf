
<div class="form-group">
    <div class="box-body">
    <div class="zone">
        <div id="dropZ">
            <i class="fa fa-cloud-upload"></i>
            <div>Drag and drop your file here</div>
            <span>OR</span>
            <div class="selectFile">
                <label for="file">Select file</label>
                <input type="file" name="files[]" id="file">
            </div>
            <p>File size limit : 10 MB</p>
        </div>
    </div>
    </div>
</div>
@section ('additionalCSS')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel='stylesheet' href='http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css'>
    <link rel="stylesheet" href="{{ route('Base')}}/css/dropzone.css">
@endsection

@section('additionalJS')
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script  src="{{ route('Base') }}/js/dropzone.js"></script>

@endsection


